# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version : 2.3.1  

* Database : PSQL  

* Rails version : 5.0.0.1  

* Test : Rspec  

* parkings_input.json is on root application  

* When click on index display table with all parkings and main attributs  
    * link New Parking open modal with form for new parking  
    * link Show parking get parking to show one parking attribut  
    * link Edit parking get page to edit attributs parking  
    * link delete parking delete parking  
    
* When click on Map get google API and locate all parkings on the map  

* When click on Import Json import all parkings on parkins_input.json file to  
database  

* When click on create output.json create output.json file on rails root with
all parkings in database  

* Heroku link : https://morning-lowlands-33565.herokuapp.com/parkings   






