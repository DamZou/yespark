class CreateParkings < ActiveRecord::Migration[5.0]
  def change
    create_table :parkings do |t|
      t.string :address
      t.boolean :available
      t.boolean :has_camera
      t.boolean :has_watchman
      t.string :city
      t.string :main_picture
      t.integer :price_month
      t.string :zip_code
      t.float :lng
      t.string :slug
      t.string :district
      t.string :name
      t.float :lat

      t.timestamps
    end
  end
end
