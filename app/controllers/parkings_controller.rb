require "json"
require "open-uri"
class ParkingsController < ApplicationController
  before_action :set_parking, only: [:show, :edit, :update, :destroy]

  # GET /parkings
  # GET /parkings.json
  def index
    @parkings = Parking.find_by_sql ["Select address, 
                                             available, 
                                             name,
                                             has_camera,
                                             has_watchman,
                                             city,
                                             main_picture,
                                             price_month,
                                             zip_code,
                                             district,
                                             id FROM parkings"]
    @parking = Parking.new
  end

  # GET /parkings/1
  # GET /parkings/1.json
  def show
  end

  # GET /parkings/new
  def new
    @parking = Parking.new
  end

  # GET /parkings/1/edit
  def edit
  end

  # POST /parkings
  # POST /parkings.json
  def create
    @parking = Parking.new(parking_params)
    latLng = latLng(@parking.address, @parking.city)
    @parking.district = district(@parking.zip_code, @parking.city)
    @parking.lat = latLng[0]
    @parking.lng = latLng[1]
    @parking.slug = @parking.name.parameterize
    respond_to do |format|
      if @parking.save
        format.html { redirect_to parkings_path notice: 'Parking was successfully created.' }
        format.json { render :show, status: :created, location: @parking }
      else
        format.html { render :new }
        format.json { render json: @parking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /parkings/1
  # PATCH/PUT /parkings/1.json
  def update
    latLng = latLng(@parking.address, @parking.city)
    respond_to do |format|
      if @parking.update(parking_params)
        @parking.update(:lat => latLng[0], 
                        :slug => @parking.name.parameterize,
                        :lng => latLng[1], 
                        :district => district(@parking.zip_code, @parking.city))
        format.html { redirect_to parkings_path, notice: 'Parking was successfully updated.' }
        format.json { render :show, status: :ok, location: @parking }
      else
        format.html { render :edit }
        format.json { render json: @parking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parkings/1
  # DELETE /parkings/1.json
  def destroy
    @parking.destroy
    respond_to do |format|
      format.html { redirect_to parkings_url, notice: 'Parking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def map
    @parkings = Parking.find_by_sql ["Select lat, 
                                             lng, 
                                             name FROM parkings"]
    gon.parkings = @parkings
  end

  def json
    root = Rails.root.to_s
    file = File.read("#{root}/parkings_input.json")
      parkings = JSON.parse(file)
      parkings.each do |parking|
        latLng = latLng(parking["address"], parking["city"])
        newParking = Parking.new(:slug => parking["name"].parameterize, 
                     :district => district(parking["zip_code"], parking["city"]), 
                     :lat => latLng[0],
                     :lng => latLng[1], 
                     :name => parking["name"], 
                     :address => parking["address"], 
                     :city => parking["city"], 
                     :zip_code => parking["zip_code"], 
                     :price_month => parking["price_month"],
                     :main_picture => parking["main_picture"], 
                     :has_watchman => parking["has_watchman"], 
                     :has_camera => parking["has_camera"], 
                     :available => parking["available"])
        newParking.remote_main_picture_url = parking["main_picture"]
        newParking.save
      end
      respond_to do |format|
        format.html { redirect_to parkings_url, notice: 'parkings_imput has been imported.' }
        format.json { head :no_content }
      end
  end

  def output
    @parkings = Parking.all
    parkings = []
    @parkings.each do |parking|
      parkings.push({:id => parking.id, 
       :lat => parking.lat,
       :lng => parking.lng,
       :name => parking.name,
       :address => parking.address,
       :available => parking.available,
       :slug => parking.slug,
       :updated_at => parking.updated_at,
       :has_camera => parking.has_camera,
       :has_watchman => parking.has_watchman,
       :zip_code => parking.zip_code,
       :district => parking.district,
       :city => parking.city,
       :main_picture => parking.main_picture,
       :price_month => parking.price_month})
    end
    File.open("output.json","w"){|f| f.write(JSON.pretty_generate(parkings))}
    respond_to do |format|
      format.html { redirect_to parkings_url, notice: 'output.json has been created.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parking
      parking = Parking.find_by_sql ["Select address,
                                             id, 
                                             available, 
                                             name,
                                             has_camera,
                                             has_watchman,
                                             city,
                                             main_picture,
                                             price_month,
                                             zip_code
                                             FROM parkings
                                             WHERE id = ?", params[:id]]
      @parking = parking.first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def parking_params
      params.require(:parking).permit(:address, :available, :has_camera, :has_watchman, :city, :main_picture, :price_month, :zip_code, :lng, :slug, :district, :name, :lat)
    end

    def latLng (address, city)
      geo = Geokit::Geocoders::GoogleGeocoder.geocode "#{address}" + ', ' + "#{city}"
      latLong = geo.ll.split(',')
    end

    def district (zip_code, city)
      district = if city == "Paris"
        zip_code.last(2)
      else
        "0"
      end
    end
end
