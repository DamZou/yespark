class Parking < ApplicationRecord

	mount_uploader :main_picture, MainPictureUploader

	validates :lat, numericality: {greater_than_or_equal_to: -90, less_than_or_equal_to: 90}
	validates :lng, numericality: {greater_than_or_equal_to: -180, less_than_or_equal_to: 180}
	validates :name, presence: true
	validates :address, presence: true
	validates :slug, presence: true
	validates :has_watchman, inclusion: [true, false]
	validates :zip_code, presence: true, format: {:with => /\A\d{5}\z/, :message => 'only five numbers' }
	validates :district, presence: true, format: {:with => /\A\d{1,2}\z/, :message => 'only one or two number(s)' }
	validates :city, presence: true
	validates :price_month, numericality: {greater_than: 0}
	
end
