Rails.application.routes.draw do
  
  resources :parkings, :except => [:new]
  get '/map', to: 'parkings#map', as: :map
  get '/json', to: 'parkings#json', as: :json
  get '/output', to: 'parkings#output', as: :output
  root :to => "parkings#index"

end