require 'rails_helper'

RSpec.describe Parking, type: :model do
  	it "slug cannot be blank." do
  		parking = createParking
  		parking[:slug] = nil
		expect(Parking.new(parking)).to_not be_valid
	end

	it "district can only be string with number." do
		parking = createParking
		parking[:district] = "be"
		expect(Parking.new(parking)).to_not be_valid
	end

	it "district is max 2 numbers." do
		parking = createParking
		parking[:district] = "123"
		expect(Parking.new(parking)).to_not be_valid
	end

	it "lat is only numerically." do
		parking = createParking
		parking[:lat] = "salut"
		expect(Parking.new(parking)).to_not be_valid
	end

	it "lat is min -90." do
		parking = createParking
		parking[:lat] = -92
		expect(Parking.new(parking)).to_not be_valid
	end

	it "lat is max 90." do
		parking = createParking
		parking[:lat] = 91
		expect(Parking.new(parking)).to_not be_valid
	end

	it "lng is only numerically." do
		parking = createParking
		parking[:lng] = "salut"
		expect(Parking.new(parking)).to_not be_valid
	end

	it "lng is min -180." do
		parking = createParking
		parking[:lng] = -181
		expect(Parking.new(parking)).to_not be_valid
	end

	it "lng is max 180." do
		parking = createParking
		parking[:lng] = 181
		expect(Parking.new(parking)).to_not be_valid
	end

	it "name cannot be blank." do
		parking = createParking
		parking[:name] = nil
		expect(Parking.new(parking)).to_not be_valid
	end

	it "address cannot be blank." do
		parking = createParking
		parking[:address] = nil
		expect(Parking.new(parking)).to_not be_valid
	end

	it "city cannot be blank." do
		parking = createParking
		parking[:city] = nil
		expect(Parking.new(parking)).to_not be_valid
	end

	it "zip code is only numerically." do
		parking = createParking
		parking[:zip_code] = "salut"
		expect(Parking.new(parking)).to_not be_valid
	end

	it "zip code is only 5 numbers." do
		parking = createParking
		parking[:zip_code] = "100"
		expect(Parking.new(parking)).to_not be_valid
	end

	it "price month is great than 0." do
		parking = createParking
		parking[:price_month] = -1
		expect(Parking.new(parking)).to_not be_valid
	end

	def createParking
		{:slug => "porte-de-saint-cloud", 
         :district => "16", 
         :lat => 48.8387038,
         :lng => 2.2576327, 
         :name => "Porte de Saint-Cloud", 
         :address => "139 rue Michel-Ange", 
         :city => "Paris", 
         :zip_code => "75016", 
         :price_month => 15900,
         :has_watchman => 0, 
         :has_camera => 0, 
         :available => 1}
	end
end
