require "rails_helper"

RSpec.describe ParkingsController, type: :routing do
  describe "ParkingsController" do
    describe "Routes" do
      it "route get map." do
        expect(:get => "/map").to route_to(:controller => "parkings",
                          :action => "map"
                          )
      end

      it "route get json." do
        expect(:get => "/json").to route_to(:controller => "parkings",
                            :action => "json"
                            )
      end

      it "route get output." do
        expect(:get => "/output").to route_to(:controller => "parkings",
                            :action => "output"
                            )
      end

      it "route get parkings." do
        expect(:get => "/parkings").to route_to(:controller => "parkings",
                            :action => "index"
                            )
      end

      it "route post parkings." do
        expect(:post => "/parkings").to route_to(:controller => "parkings",
                            :action => "create"
                            )
      end

      it "route get parkings/:id/edit." do
        expect(:get => "/parkings/:id/edit").to route_to(:controller => "parkings",
                            :action => "edit",
                            :id => ":id"
                            )
      end

      it "route get parkings/:id." do
        expect(:get => "/parkings/:id").to route_to(:controller => "parkings",
                            :action => "show",
                            :id => ":id"
                            )
      end

      it "route patch parkings/:id." do
        expect(:patch => "/parkings/:id").to route_to(:controller => "parkings",
                            :action => "update",
                            :id => ":id"
                            )
      end

      it "route put parkings/:id." do
        expect(:put => "/parkings/:id").to route_to(:controller => "parkings",
                            :action => "update",
                            :id => ":id"
                            )
      end

      it "route delete parkings/:id." do
        expect(:delete => "/parkings/:id").to route_to(:controller => "parkings",
                            :action => "destroy",
                            :id => ":id"
                            )
      end
    end

  end
end
